## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This is a website displaying my coding assignments for ME 305
#
#  @section sec_hom Homework 00
#  This is an example of implementing a Finite State Machine using an Elevator as the object. Please see Elevator_FSM.py which is part of the \ref elevator_fsm package.
#
#  @section sec_fib Fibbonacci Sequence (Lab 1)
#  This generates a Fibbonacci Sequence based off of user input. Please see fibb_gen.py which is part of the \ref fibb_gen package.
#
#  @author Jacob Burghgraef
#
#  @date October 9, 2020
#