'''
@file ClosedLoop.py

@author Jacob Burghgraef

@date November 24th, 2020

@brief This file is a class that calculates an actuation value

The documentation for this file can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/ClosedLoop.py
'''

class ClosedLoop():
    '''
    @brief      The Closed Loop class calculates the actuation value.
    '''
    def __init__ (self):
        '''
        @brief      Creates a closed loop object.
        @details    Sets up the closed loop class by zeroing the measured 
                    motor velocity, reference motor velocity, actuation value,
                    and proportional gain value. Note that this value is the 
                    Kp' value referred to in lecture in which the voltage of 
                    the motor is already taken into account. This simplifies 
                    the calculation and this class considerably.
        '''        
        ## The measured angular velocity of the motor
        self.Omega_meas = 0
        
        ## The reference angular velocity of the motor
        self.Omega_ref = 0
        
        ## The actuation value
        self.L = 0
        
        ## The proportional gain value
        self.Kp = 0 
    
    def update(self, Omega_ref, Omega_meas):
        '''
        @brief      Calculates and returns the actuation value
        @param Omega_ref    The reference angular velocity value
        @param Omega_meas   The measured angular velocity value
        @return L   The actuation value
        '''
        
        # Get the reference angular velocity of the motor
        self.Omega_ref = Omega_ref    
        
        # Get the measured angular velocity of the motor
        self.Omega_meas = Omega_meas
        
        # Calculate the actuation value
        #self.L = int(self.Kp*(self.Omega_ref - self.Omega_meas))
        self.L = int(self.Kp*(self.Omega_ref))
        
        return self.L
    
    def setKp(self, Kp):
        '''
        @brief      Sets the Kp value
        @param Kp   The initial proportional gain value, given by the user
        '''
        # Set the new proportional gain value
        self.Kp = Kp
        
    def getKp(self):
        '''
        @brief      Retrieves the Kp value
        @return Kp  The initial proportional gain value, given by the user
        '''
        return self.Kp