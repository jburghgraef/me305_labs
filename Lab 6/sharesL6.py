'''
@file sharesL6.py
@package Lab_6
@brief A container for all the inter-task variables
@author Jacob Burghgraef, modified from a file created by Charlie Refvem
@date December 4th, 2020
@copyright This work is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. https://creativecommons.org/licenses/by-nc-sa/4.0/
'''

## The command character sent from the user interface task to the controller task
cmd     = None

## The response from the controller task after collecting data
resp    = None