'''
@file Task_Controller.py
@package Lab_6
@brief Task that controls a motor using a closed control loop.
@details This file is a Finite State Machine that acts as a backend for the 
         UI_FrontEnd.py file, this class controls the timing of the closed
         control loop by calling to ClosedLoop.py
@author Jacob Burghgraef
@date November 24th, 2020

Below is an image of the Finite State Machine describing this file
@image html "ME 305 Lab 6 Task_Controller FSM.jpg"

Below is an image of the Class Diagram describing Lab 6
@image html "Lab 6 Task Diagram.jpg"

The source code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/Task_Controller.py
'''

from ClosedLoop import ClosedLoop
from Encoder_Driver_Lab6 import EncoderDriver
from Motor_Driver import MotorDriver
import pyb
import sharesL6
import utime

class Controller():
    '''
    @brief      A finite state machine that controls a closed loop
    '''
    
    ## Constant Defining the Initial State
    S0_INIT =                   0
    
    ## Constant Defining State 1
    S1_WAIT_FOR_KP =            1
    
    ## Constant Defining State 2
    S2_LOOP =                   2
    
    def __init__(self, interval):
        '''
        @brief      Creates a Controller Object
        '''
    
        ## The interval for the closed loop
        self.interval = int(interval*1e6) # Received in microseconds
        
        # Create the pin objects used for interfacing with the encoder driver
        self.pin1 = pyb.Pin(pyb.Pin.cpu.B6)
        self.pin2 = pyb.Pin(pyb.Pin.cpu.B7)
        
        # Create the timer object used for the encoder
        self.tim_enc = pyb.Timer(4, prescaler = 0, period = 0xFFFF)
        
        ## The Encoder object
        self.enc = EncoderDriver(self.pin1, self.pin2, self.tim_enc, self.interval)
        
        # Create the pin objects used for interfacing with the motor driver
        self.pin_nSLEEP = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
        self.pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4)
        self.pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5)

        # Create the timer object used for PWM generation
        self.tim_mot = pyb.Timer(3, freq = 20000)
        
        ## The Motor object
        self.mot = MotorDriver(self.pin_nSLEEP, self.pin_IN1, self.pin_IN2, self.tim_mot)
        
        ## The closed loop object
        self.ClosedLoop = ClosedLoop()
        
        ## The reference Omega value
        self.Omega_ref = 1000 # In RPM
        
        ## The counter for the number of data points
        self.i = 0
        
        ## Start at the initial state of the FSM
        self.state = self.S0_INIT
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_KP)
            
            elif (self.state == self.S1_WAIT_FOR_KP):
                # Checks if a Kp value has been received by the user task
                # If so, then the Kp value is set to the closed loop class
                # and the motor is enabled
                
                if sharesL6.cmd:
                    #print('The command received by the Controller task is ' + str(shares.cmd))
                    # Set the Kp value 
                    self.ClosedLoop.setKp(float(sharesL6.cmd))
                    # Enable the motor
                    self.mot.enable()
                    # Reset the Runs counter
                    self.i = 0
                    # Transition to state 2
                    self.transitionTo(self.S2_LOOP)
                
            elif (self.state == self.S2_LOOP):
                # This is the control loop for collecting motor data; first,
                # the encoder position is updated, then the angular velocity 
                # is measured, then the actuation value is calculated and set 
                # to the motor's duty cycle, and finally, the measured 
                # angular velocity is sent to the task user for storage
                
                # Get the measured speed from the encoder
                self.enc.update()
                
                self.Omega_meas = self.enc.getSpeed()
                
                # Increment up the loop counter
                self.i += 1
                
                # Get the calculated actuation value from the closed loop class
                self.calculated_L = self.ClosedLoop.update(self.Omega_ref, self.Omega_meas)
                
                # Set the new duty cycle
                self.mot.set_duty(self.calculated_L)
                sharesL6.resp = self.Omega_meas
                
                # After collecting 150 data points, delete the k value and stop the motor
                if self.i >= 150:
                    sharesL6.cmd = None
                    self.transitionTo(self.S1_WAIT_FOR_KP)
                    self.mot.disable()
            else:
                pass
                
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
            
            
            
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState