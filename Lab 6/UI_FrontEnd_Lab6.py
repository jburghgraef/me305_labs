'''
@file       UI_FrontEnd_Lab6.py
@package    Lab_6
@brief      Creates a UI for Reading and Plotting Motor Data in Spyder
@details    This file sets up the serial port and lists for communication with 
            the Nucleo, and in an infinite loop queries the user for a kp 
            value, sends that value to the Nucleo and waits for data to be 
            collected, then parses the collected data into the predesignated 
            lists for plotting, plots the data and the asks the user if they
            want to collect more data. Should the user respond with 'Y' for 
            yes, the program will restart automatically. Otherwise, the serial
            port is closed and the program terminates itself.
@author     Jacob Burghgraef
@date       December 4th, 2020

The following 5 plots depict attempts at invoking a step response with varying
Kp values. Note that the static reference velocity for each of these plots is 
1000 RPMs. The tendency towards higher velocity outputs with increasing Kp 
values indicates that there is an issue with one or more of the following: 
        1) Encoder_Driver_Lab6.py
        The Encoder Driver, which might be reading positions incorrectly,
        calculating deltas incorrectly, or converting units from ticks per
        microsecond to RPMs incorrectly. In any case, I have reviewed the file
        for each of these errors, and am unable to find which, if any, is 
        causing the error.
        2) Motor_Driver.py
        The Motor Driver, which could be setting duty cycles incorrectly. This
        seems highly unlikely because my motor driver class works just fine on
        its own.
        3) Closed_Loop.py
        The calculation of the actuation value could be incorrect due to
        improper implementation of the control loop proportional gain equation.
        This seems probable, but unlikely, since the equation matches the one
        given in the lab handout.
        4) Task_Controller.py
        The controller task could be at fault due to some unforseen algorithmic
        error or syntax error somewhere in the code.
        5) The Hardware
        It's possible that my hardware is defective in some way. This seems 
        unlikely since I verified the encoder's output signal and motor's 
        input signal with an oscilloscope.
Additionally, it appears that as the Kp values approach .05, more variation in 
the motor's output can be observed. I would have to guess that this is due to
an undershoot of the actuation value due to it being too low. As the Kp value 
approaches .09, this variation becomes much less prevalent. Increasing the Kp 
value much beyond .09 leads the actuation value to grow to be greater than 100,
thus throwing an error in the motor driver. This evidence lends credence to the
idea that the calculation of the actuation value is incorrect since the 
calculated actuation value doesn't 'oscillate' about the correct duty cycle
corresponding to the reference velocity.

@image html "Lab 6 kp = 0.05.png"
The response at Kp = 0.05

@image html "Lab 6 kp = 0.06.png"
The response at Kp = 0.06

@image html "Lab 6 kp = 0.07.png"
The response at Kp = 0.07

@image html "Lab 6 kp = 0.08.png"
The response at Kp = 0.08

@image html "Lab 6 kp = 0.09.png"
The response at Kp = 0.09

The source code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/UI_FrontEnd_Lab6.py
'''

import serial
import matplotlib.pyplot as plt
import time as frontend
    
## The serial object used to communicate with the Nucleo in Spyder
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
        
## The list of timestamp values
time = []
        
## The list of positon values from the encoder
omega = [] 

def sendKp():
        '''
        @brief Sends an ascii character to the Nucleo
        @return myval An integer representing the ascii character
        '''
        inv = input('Send a Kp value: ')
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline().decode('ascii')
        return myval
   
def ExtractDataPacket():
        '''
        @brief Extracts position and timestamp values from Nucleo
        @details This uses the serial package to read the data written to the
                 local COM port from the nucleo. It then saves the comma 
                 separated values into a csv array line-by-line, and finally  
                 strips each line of values and partitions them into a  
                 corresponding position array and time array as integers.
        '''
        n = 0
        
        ## The size of the incoming array of values
        size = 0
        
        # Wait 4 seconds to collect data
        frontend.sleep(4)
        
        while True:
            line = ser.readline()
    
            # If the line is empty, there are no more rows so exit the loop
            if line == '':
                break
    
            # If the line is not empty, increment the counter
            else:
                size += 1
        
        for n in range(size):
            ## The string representing a single row of data from the data collection task
            line_string = ser.readline().decode('ascii')
            
            ## A list representing the extracted strings of the collected data
            line_list = line_string.strip().split(',')
            
            # Add the integer timestamp value to the time array
            time.append(int(line_list[0])/1e6)
            
            # Add the integer position value to the position array
            omega.append(int(line_list[1]))
            
        
def PlotData():
        '''
        @brief Plots the data using matlibplot
        '''
        # Plot the omega values vs time values
        plt.plot(time, omega)
        
        # Label the x-axis, y-axis and title of the plot
        plt.title('Encoder Angular Velocity vs. Time')
        plt.xlabel('Timestamp (in Seconds)')
        plt.ylabel('Encoder Angular Velocity (in RPM)')
        
        # Make sure the plot is visible
        plt.show()

def CloseSerialPort():
        '''
        @brief Closes the Serial Port
        '''    
        ser.close()

# Loop to make sure the user can see 
while True:
    # Query User for an input
    sendKp()
            
    # Extract the data the Collection task wrote to the serial port
    ExtractDataPacket()
            
    # Plot that data
    PlotData()
            
    # Ask the user if they want to collect data again
    inp = input('Collect more data? (Y/N): ')
            
    if (inp == 'N'):
        # close serial port and end UI
        CloseSerialPort()
        print('Data collection progam terminated by user.')
        break
    elif (inp == 'Y'):
        # clear prior data
        line_list = []
        line_string = None
        omega = []
        time = []
    else:
        # Error Case: Invalid input
        print('Invalid response; great job, you broke it.')
        print('This program will now self-terminate. Goodbye.')
        CloseSerialPort()
        break;