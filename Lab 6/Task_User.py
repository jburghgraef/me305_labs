'''
@file Task_User.py
@package Lab_6
@brief A User interface task.
@details This is a user interface task that sends measured motor speed values 
         and their corresponding time stamps from Task_Controllor.py to 
         UI_FrontEnd_Lab6.py; it also receives kp values from the frontend UI.

@author Jacob Burghgraef

@date November 24, 2020
 
Below is an image of the Finite State Machine describing this file
@image html "ME 305 Lab 6 Task_User FSM.jpg"

The source code for this file can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%206/Task_User.py
'''

import sharesL6
from pyb import UART
import utime
from array import array

class TaskUser:
    '''
    User interface task.
    '''
    
    ## Initialization state
    S0_INIT             = 0
    
    ## Waitign for character state
    S1_WAIT_FOR_CMD     = 1
    
    ## Waiting for response state
    S2_WAIT_FOR_RESP    = 2
    
    def __init__(self, interval):
        '''
        Creates a TaskUser object.
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The time array
        self.t = array('i')
        
        ## The array of angular velocities
        self.w = array('i')
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        ## The time at the beginning of Data Collection
        self.collect_time = 0
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif (self.state == self.S1_WAIT_FOR_CMD):
                if self.ser.any():
                    self.transitionTo(self.S2_WAIT_FOR_RESP)
                    sharesL6.cmd = self.ser.readline().decode('ascii')
                    self.collect_time = utime.ticks_add(self.curr_time, self.interval)
                    #print('The command sent by the user task is: ' + str(shares.cmd))
                    
            elif (self.state == self.S2_WAIT_FOR_RESP):
                #print('The response received by the user task is: ' + str(shares.resp))
                if sharesL6.resp:
                    # Append the current time to the time array
                    self.t.append(utime.ticks_diff(self.curr_time, self.collect_time))
                    # Append the measured angular velocity to the velocity array
                    self.w.append(int(sharesL6.resp))
                    # Write the time value and omega value to the serial port
                    self.ser.write('{:}, {:}\r\n'.format (self.t[-1], self.w[-1]))
                    
                    # The following line could break the code, just saying
                    
                    # Reset the shares.resp value
                    sharesL6.resp = None
                    
                if (sharesL6.cmd == None):
                    self.transitionTo(self.S1_WAIT_FOR_CMD)
                    self.collect_time = 0
                
            else:
                # Invalid state case, error occurs
                pass
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState