'''
@file main.py

A Main Script for running Lab 6

@author Jacob Burghgraef

@date November 24, 2020
'''

from Task_Controller import Controller
from Task_User import TaskUser

## Create a User task object
task1 = TaskUser(.02) # Interval of 20 milliseconds

## Create a Controller task object
task2 = Controller(.02) # Time step of 20 milliseconds

while True:
    task1.run()
    task2.run()