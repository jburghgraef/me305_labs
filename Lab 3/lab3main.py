'''
@file lab3main.py

@author Jacob Burghgraef

@date November 17, 2020

@brief The main script for lab 3

@details This file serves as the main script for running the scripts of lab 3.
        Those scripts include Encoder_Driver.py, TaskEncoder.py, and TaskUI.py

The documentation for this file can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%203/lab3main.py
'''

from TaskEncoder import TaskEncoder
from TaskUserInterface import TaskUI

## Creates a TaskEncoder object that runs every tenth of a second
task1 = TaskEncoder(0.1)

## Creates a TaskUI object and sets it to a variable to run the task
task2 = TaskUI()

## Run tasks in perpetuity 
while True:
    task1.runTaskEncoder()
    task2.runTaskUI()