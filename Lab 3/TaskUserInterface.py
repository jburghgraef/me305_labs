'''
@file TaskUserInterface.py

@author Jacob Burghgraef

@date November 17, 2020

@brief This file is a task that creates a user interface.

@details This task is a finite state machine that checks for a command, reads
         that command, and then executes the command. This FSM is a UI that 
         interacts cooperatively with the file TaskEncoder.py /ref
         
The FSM for this file can be found below:
    
@image html "ME 305 Lab 3 Task User Interface FSM.jpg"

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%203/TaskUserInterface.py
'''

from pyb import UART
from Encoder_Driver import encoderdriver

class TaskUI:
    '''
    @brief      A finite state machine that creates a User Interface.
    @details    This class implements a finite state machine to generate a
                user interface that cooperatively sends commands to 
                TaskEncoder.py
                These commands include:
                    z - Zeroes the Encoder Position
                    p - Prints the Encoder Position
                    d - Prints the Encoder Delta
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_CHECK_INPUT      = 1
    
    ## Constant defining State 2
    S2_READ_INPUT       = 2
    
    ## Constant defining State 3
    S3_EXECUTE_COMMAND  = 3
    
    def __init__(self):
        '''
        @brief      Creates a TaskUserInterface object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
    
        ## The serial communitcation bus
        self.uart = UART(2)
        
        ## Initialize check of number of waiting input bytes to equal 0
        self.check = 0
        
        ## Initialize the integer variable for the user input
        self.input = 0
        
        ## The Encoder driver object
        self.enc = encoderdriver(3)
        
    def runTaskUI(self):
        '''
        @brief      Runs one iteration of the task
        '''
        
        if (self.state == self.S0_INIT):
            print('Welcome to the Encoder UI!')
            print('The commands are as follows: ')
            print('z - Zeroes the Encoder Position')
            print('p - Prints the Encoder Position')
            print('d - Prints the Encoder Delta')
            print('h - Prints help text block')
            self.transitionTo(self.S1_CHECK_INPUT)
        
        elif (self.state == self.S1_CHECK_INPUT):
            # Check for any input
            self.check = self.uart.any()
            if (self.check):
                self.transitionTo(self.S2_READ_INPUT)
            else: 
                pass
        
        elif (self.state == self.S2_READ_INPUT):
            # Set whatever character is in the UART bus to the integer input
            self.input = self.uart.readchar()
            self.transitionTo(self.S3_EXECUTE_COMMAND)

        elif (self.state == self.S3_EXECUTE_COMMAND):
            if (self.input == 122):
                # cmd = z --> Zero the Encoder
                print('Encoder zeroed.')
                self.enc.set_position(0)
                
            elif (self.input == 112):
                # cmd = p --> Print Encoder position
                self.p = self.enc.get_position()
                print('Encoder is at position: ' + str(self.p))
                
            elif (self.input == 100):
                # cmd = d --> Print Encoder Delta
                self.d = self.enc.get_delta()
                print('Encoder delta is: ' + str(self.d))
            
            elif (self.input == 104):
                # cmd = h --> Help; print help text block
                print('The commands are as follows: ')
                print('z - Zeroes the Encoder Position')
                print('p - Prints the Encoder Position')
                print('d - Prints the Encoder Delta')
                print('h - Prints help text block')
                
            else:
                # Unrecognizable input
                print('That command undefined; for a full list of commands,')
                print('please type h for the help textbox.')
                pass
            
            # Subtract 1 from check, indicating that one of the waiting 
            # waiting commands has been executed.
            self.check =- 1
            
            if (self.check <= 0):
                # No more cmds to execute, reset check and go back to state 1
                self.check = 0
                self.transitionTo(self.S1_CHECK_INPUT)
            else:
                # Read the next command
                self.transitionTo(self.S2_READ_INPUT)
            
        else:
            pass
                        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState