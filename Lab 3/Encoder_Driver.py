'''
@file Encoder_Driver.py

@author Jacob Burghgraef

@date November 17, 2020

@brief This file is the driver for an encoder

@details This file is the driver for an encoder; it can retrieve the encoder's 
         current position, set the encoder's current position to a certain 
         value, update the current position, and retrieve the difference 
         between the current position and the most recent position.

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%203/Encoder_Driver.py
'''

import pyb

class encoderdriver:
    '''
    @brief      A driver class with a few basic functions for an encoder.
    @details    This driver class uses the pyb pin function and timer function
                to create a driver for an encoder. The functions within this 
                driver include update(), which reads the encoder's position, 
                getposition(), which retrieves the encoder's positon, 
                setposition(), which sets the encoder's position, and 
                getdelta(), which retrieves the difference between the two
                most recent position values in update.
    '''
    
    def __init__ (self, timer_id):
        '''
        @brief      Creates an encoderdriver object.
        @details    This constructor sets up the encoderdriver object by 
                    defining the pin the encoder connects to and the timer
                    channel to be used in reading from the encoder.
        @param      The integer timer id
        '''
        
        ## The timer id for the type of timer channel
        self.timer_id = timer_id
        
        ## Create 2 'pin' variables representing the pins the encoder connects to on the Nucleo
        self.pin1 = pyb.Pin(pyb.Pin.cpu.A6)
        self.pin2 = pyb.Pin(pyb.Pin.cpu.A7)
        
        ## Position of encoder - initialized at zero
        self.position = 0
        
        ## Storage variable for calculation of the delta value - initialized at zero
        self.old_position = 0
        
        ## Represents the output of the delta calculation - initialized at zero
        self.delta = 0
        
        ## Create a timer channel with the user-specified timer type 
        while True:
            if int(self.timer_id) == 2 or int(self.timer_id) == 5:
                self.tim = pyb.Timer(int(self.timer_id))
                self.tim.init(prescaler = 0, period = 0x3FFFFFFF)
                break
            elif (int(self.timer_id) != 2 or int(self.timer_id) != 5) and (int(self.timer_id) > 1 or int(self.timer_id) < 15) :
                self.tim = pyb.Timer(int(self.timer_id))
                self.tim.init(prescaler = 0, period = 0xFFFF)
                break
            else:
                self.timer_id = input('Invalid timer id, please input an integer between 1 & 15.')
        
        ## Initialize timer channels     
        self.tim.channel(1, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(2, pin = self.pin2, mode = pyb.Timer.ENC_AB)
        
    def update(self):
        '''
        @brief      Obtains the current reading from the Encoder.
        @details    This obtains the current reading from the encoder, and
                    it calculates the change in position between the current
                    reading and the most recent reading, otherwise known as 
                    the 'delta.' update() then checks for overflow and 
                    underflow in the delta values, and then corrects it.
        '''  
        
        # Set the old position to the position from the last loop
        self.old_position = self.position
        
        # Set the position to the current counter reading
        self.position = self.tim.counter()
        
        # Calculate delta
        self.delta = self.position - self.old_position
        
        ## Period for Checking Overflow and Underflow
        self.per = 0.5*self.tim.period()
        
        # Check for Overflow and Underflow
        if (abs(self.delta) > self.per):
            if (self.delta < 0):
                # Correct Underflow
                self.delta = self.delta + self.tim.period()
            elif (self.delta > 0):
                # Correct Overflow
                self.delta = self.delta - self.tim.period()
            else:
                pass
        else:
            pass
    
    def get_position(self):
        '''
        @brief      Obtains the position from update() and returns the position.
        @return     An integer value representing the current position of
                    the encoder.
        '''  
        self.update()
        return int(self.position)
    
    def set_position(self, pos):
        '''
        @brief      Sets current position of Encoder to a specified value.
        @param      The user inputted position.
        '''  
        self.position = self.tim.counter(pos)
        
    def get_delta(self):
        '''
        @brief      Obtains the delta value.
        @details    Obtains the delta value calculated and checked in update().
        @return     Integer value of delta.
        '''      
        self.update();
        return self.delta