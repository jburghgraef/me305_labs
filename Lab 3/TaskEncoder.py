'''
@file TaskEncoder.py

@author Jacob Burghgraef

@date November 17, 2020

@brief This file is a task that updates an encoder.

@details This file is a finite state machine that employs the 
         Encoder_Driver.py /ref file to update an encoder continuously.
                  
The FSM diagram for this Task can be found below:

@image html "ME 305 Task Encoder FSM.jpg"

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%203/TaskEncoder.py
'''

from Encoder_Driver import encoderdriver
import utime

class TaskEncoder:
    '''
    @brief      A finite state machine to read from an Encoder.
    @details    This class implements a finite state machine to read from an
                encoder at a regular interval.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_UPDATE_ENCODER   = 1
    
    def __init__(self, interval):
        '''
        @brief      Creates a TaskEncoder object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## An Encoder Driver object
        self.enc = encoderdriver(3)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def runTaskEncoder(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
            
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_UPDATE_ENCODER)
                
            elif(self.state == self.S1_UPDATE_ENCODER):
                
                # Update the Encoder
                self.enc.update();
                
            else:
                # Uh-oh state (undefined state)
                # Error handling
                pass
            
            # updating the "Scheduled" timestamp
            self.next_time = utime.ticks_add(self.start_time, self.interval)
            
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState              