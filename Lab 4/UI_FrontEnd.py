'''
@file UI_FrontEnd.py
@brief Creates a UI for Reading Encoder Data in Spyder
@author Jacob Burghgraef
@date November 3, 2020

The source code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%204/UI_FrontEnd.py
'''

import serial
import matplotlib.pyplot as plt
import numpy as np
import time as frontend
from array import array
import keyboard
    
## The serial object used to communicate with the Nucleo in Spyder
ser = serial.Serial(port='COM4',baudrate=115273,timeout=1)
        
## The list of timestamp values
time = np.zeros(51)
        
## The list of positon values from the encoder
position = np.zeros(51)

## The list of strings representing all data sent from the data collection task
csv = np.array(array)

def sendChar():
        '''
        @brief Sends an ascii character to the Nucleo
        @return myval An integer representing the ascii character
        '''
        inv = input('Collect Data? (G == Start Collection, S == Stop Collection): ')
        ser.write(str(inv).encode('ascii'))
        myval = ser.readline().decode('ascii')
        return myval
   
def ExtractDataPacket():
        '''
        @brief Extracts position and timestamp values from Nucleo
        @details This uses the serial package to read the data written to the
                 local COM port from the nucleo. It then saves the comma 
                 separated values into a csv array line-by-line, and finally  
                 strips each line of values and partitions them into a  
                 corresponding position array and time array as integers.
        '''
        n = 0
        t = frontend.time()
        while True:
            if (keyboard.is_pressed('S')):
                print('Data collection stopped by user.')
                break;
            elif (frontend.time() > t + 10):
                print('All data collected, extracting data packet...')
                break;
            else:
                pass
                
        for n in range(51):
            ## The string representing a single row of data from the data collection task
            line_string = ser.readline().decode('ascii')
            print(line_string)
            
            ## A list representing the extracted strings of the collected data
            line_list = line_string.strip().split(',')
            print(line_list)
            
            # Add the integer timestamp value to the time array
            time[n] = int(line_list[0])/1e6
            
            # Add the integer position value to the position array and convert from encoder ticks to degrees
            position[n] = int(line_list[1])*360/1400
            
            # Add line_list to the csv array
            csv[n] = str(line_list)
            
            

def SaveDatainCSV():
        '''
        @brief Saves the data as a csv
        '''
        np.savetxt('lab4values.csv', csv, delimiter=',')
        
def PlotData():
        '''
        @brief Plots the data using matlibplot
        '''
        plt.plot(time, position)
        plt.title('Encoder Position vs. Time')
        plt.xlabel('Timestamp (in Seconds)')
        plt.ylabel('Encoder Position (in Degrees)')
        plt.show()

def CloseSerialPort():
        '''
        @brief Closes the Serial Port
        '''    
        ser.close()


while True:
    # Query User for an input
    sendChar()
            
    # Extract the data the Collection task wrote to the serial port
    ExtractDataPacket()
            
    # Plot that data
    PlotData()
            
    # Save that data in a csv file
    SaveDatainCSV()
            
    # Ask the user if they want to collect data again
    inp = input('Collect more data? (Y/N): ')
            
    if (inp == 'N'):
        CloseSerialPort()
        print('Data collection progam terminated by user.')
        break;
    elif (inp == 'Y'):
        pass
    else:
        print('Invalid response; great job, you broke it.')
        print('This program will now self-terminate. Goodbye.')
        CloseSerialPort()
        break;