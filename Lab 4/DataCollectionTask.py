'''
@file DataCollectionTask.py
@brief Task that collects data from an encoder
@details This file is a Finite State Machine that acts as a backend for the 
         UI_FrontEnd.py file. It collects data from an encoder via the 
         Encoder_Driver.py file and writes that data along with its 
         timestamps to the serial port.
@author Jacob Burghgraef
@date November 3, 2020

Below is an image of the Finite State Machine describing this file
@image html "ME 305 Lab 4 FSM.jpg"

Below is an image of the Class Diagram describing Lab 4
@image html "ME 305 Lab 4 Class Diagram.jpg"

The source code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%204/DataCollectionTask.py
'''

from array import array
from pyb import UART
import utime
from Encoder_Driver import encoderdriver

class DCTask():
    '''
    @brief      A finite state machine that collects data from an encoder
    '''
    
    ## Constant Defining the Initial State
    S0_INIT =           0
    
    ## Constant Defining State 1
    S1_WAIT_FOR_CMD =   1
    
    ## Constant Defining State 2
    S2_EXECUTE_CMD =    2
    
    def __init__(self, interval, sample_duration):
        '''
        @brief      Creates a DCTask object
        @details    The DCTask (Data Collection Task) object stores data from
                    an encoder.
        '''
        
        ## The Encoder driver object
        self.enc = encoderdriver(3)
        
        ## Time interval between Data Samplings, in microseconds
        self.interval = int(interval*1e6)
        
        ## The amount of time the task will collect data
        self.sample_duration = sample_duration
        
        ## Start at the initial state of the FSM
        self.state = self.S0_INIT
        
        ## Value representing the number of rows of data points
        self.rows = sample_duration/interval
        
        ## The array of position values collected from the encoder
        self.x = array('i')
        
        ## The array of timestamp values corresponding to positions during data collection
        self.t = array('i')
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## Counter used to index data collection points
        self.i = 0
        
        ## Serial port
        self.ser = UART(2)
        
        self.calltoCollect = 0
        
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            #print('current time: ' + str(self.curr_time))
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_CMD)
                
            elif (self.state == self.S1_WAIT_FOR_CMD):
                if (self.ser.any() != 0):
                    cmd = self.ser.readchar()
                    if cmd == 71:
                        # User inputs 'G' to initiate data collection
                        self.transitionTo(self.S2_EXECUTE_CMD)
                        print('Begininng Data collection...')
                    elif cmd == 83:
                        # User inputs 'S' to stop data collection
                        print('No Data Collection to Stop, but Okay.')
                    else:
                        pass
                        # Invalid input case
                        #print('Invalid command, please type G to start Data Collection or S to stop Data Collection.')
                    
            elif (self.state == self.S2_EXECUTE_CMD):
                # Check if user wants to stop data collection
                if (self.ser.any() != 0):
                    cmd = self.ser.readchar()
                    if cmd == 83:
                        # User inputs 'S' to stop data collection
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                        #print('Data Collection Terminated by User.')
                        #print('Number of data points collected: ' + str(self.i))
                        #print('Time Elapsed: ' + str((self.i)*self.interval/1e6) + ' seconds')
                        self.i = 0
                    else:
                        # Invalid input case
                        pass
                        #print('Invalid command, if you want to stop data collection, please type S.')
                else:
                    # Collect data points at a sampling rate of 5 hz for 10 seconds (for Lab 04)
                    self.collect()
                
                    # Increment up data collection counter
                    self.i += 1
                    #print('i is ' + str(self.i))
                    # Check if 50 data points have been collected
                    if (self.i > self.rows):
                        #print('Data Collection Finished.')
                        self.i = 0
                        self.transitionTo(self.S1_WAIT_FOR_CMD)
                    else:
                        pass
                        #print('Collected Data Point ' + str(self.i))
            else:
                pass
        
            self.runs =+ 1
        
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
        
    def collect(self):
        '''
        @brief  Collects 1 Data Point from the Encoder
        @param  index An index value representing the location of a data point
                in the array.
        '''
        self.enc.update()
        self.x.append(self.enc.get_position())
        self.t.append((self.curr_time))
        self.ser.write('{:}, {:}\r\n'.format (self.t[self.i], self.x[self.i]))      
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run.
        '''
        self.state = newState