'''
@file main.py
@brief The code that runs the Data Collection task on the nucleo
@author Jacob Burghgraef
@date November 3, 2020
'''

from DataCollectionTask import DCTask

task = DCTask(.2, 10)

while True:
    task.run()