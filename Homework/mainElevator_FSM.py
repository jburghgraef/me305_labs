'''
@file mainElevator_FSM.py

@author Jacob Burghgraef

@date October 9, 2020

This file serves as a main script running the Elevator_FSM.py file

The source code can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Homework/mainElevator_FSM.py
'''
from Elevator_FSM import Button, MotorDriver, TaskElevator


# Objects for task_a

## Motor Object
Motor_a = MotorDriver()

## Button Object for "button_1"
button_1_a = Button('PB6')

## Button Object for "button_2"
button_2_a = Button('PB7')

## Button Object for "first"
first_a = Button('PB8')

## Button Object for "second"
second_a = Button('PB9')

# Objects for task_b

## Motor Object
Motor_b = MotorDriver()

## Button Object for "button_1"
button_1_b = Button('PB6')

## Button Object for "button_2"
button_2_b = Button('PB7')

## Button Object for "first"
first_b = Button('PB8')

## Button Object for "second"
second_b = Button('PB9')

## Task Object
task_a = TaskElevator(Motor_a, button_1_a, button_2_a, first_a, second_a) # Will also run Constructor
task_b = TaskElevator(Motor_b, button_1_b, button_2_b, first_b, second_b)

# Runs Each Task 1000 times
for N in range(1000): 
    task_a.run()
    task_b.run()