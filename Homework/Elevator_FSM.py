'''
@file Elevator_FSM.py

@author Jacob Burghgraef

@date October 9, 2020

This file serves as an example implementation of a finite-state-machine using
Python. The example will implement some code to control an imaginary elevator.

The fintie-state-machine or 'FSM' for this elevator is depicted below.

@image html ElevatorFSM.JPG

The user has a button to go to floor 1 and a button to go to floor 2.

There is also an indicator of which floor the user is on.

The source code can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Homework/Elevator_FSM.py
'''

from random import choice

class TaskElevator:
    '''
    @brief      A finite state machine to control of an Elevator.
    @details    This class implements a finite state machine to control the
                operation of an Elevator.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT                 = 0
    
    ## Constant defining State 1
    S1_MOVING_DOWN          = 1
    
    ## Constant defining State 2
    S2_MOVING_UP            = 2
    
    ## Constant defining State 3
    S3_STOPPED_AT_FLOOR_1   = 3
    
    ## Constant defining State 4
    S4_STOPPED_AT_FLOOR_2   = 4
    
    def __init__(self, Motor, button_1, button_2, first, second):
        '''
        @brief      Creates a TaskElevator object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## A class attribute "copy" of the motor object
        self.Motor = Motor # Stores class copy of Motor so other functions can
                           # use the Motor object
        
        ## A class attribute "copy" of the button_1 object
        self.button_1 = button_1
        
        ## A class attribute "copy" of the button_2 object
        self.button_2 = button_2
        
        ## The button object used for the first floor
        self.first = first
        
        ## The button object used for the second floor
        self.second = second
        
        ## A counter showing the number of times the task has run
        self.runs = 0
    
    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        if(self.state == self.S0_INIT):
            print('Run: ' + str(self.runs) + ' State 0: Initializing')
            # Run State 0 Code
            self.transitionTo(self.S1_MOVING_DOWN)
            self.button_1.On()
            self.button_2.clear()
            self.first.On()
            self.second.clear()
            
        elif(self.state == self.S1_MOVING_DOWN):
            print('Run: ' + str(self.runs) + ' State 1: Elevator Moving Down')
            # Run State 1 Code
            self.second.clear()    # No longer on the Second Floor
            self.transitionTo(self.S3_STOPPED_AT_FLOOR_1)
            self.Motor.Move_Down()
        
        elif(self.state == self.S2_MOVING_UP):
            print('Run: ' + str(self.runs) + ' State 2: Elevator Moving Up')
            # Run State 2 Code
            self.first.clear()      # No longer on the First Floor
            self.transitionTo(self.S4_STOPPED_AT_FLOOR_2)
            self.Motor.Move_Up()
            
        elif(self.state == self.S3_STOPPED_AT_FLOOR_1):
            print('Run: ' + str(self.runs) + ' State 3: Elevator at Floor 1')
            # Run State 3 Code
            self.button_1.clear()   # Reset Button 1
            self.first.On()         # On the First Floor
            
            # if button_2 is pushed (true)
            if self.button_2.getButtonState():
                self.transitionTo(self.S2_MOVING_UP)
                self.Motor.Stop()
            
        elif(self.state == self.S4_STOPPED_AT_FLOOR_2):
            print('Run: ' + str(self.runs) + ' State 4: Elevator at Floor 2')
            # Run State 4 Code
            self.button_2.clear()   # Reset Button 2
            self.second.On()        # On the Second Floor
                
            # if button_1 is pushed (true)
            if self.button_1.getButtonState():
                self.transitionTo(self.S1_MOVING_DOWN)
                self.Motor.Stop()
            
        else:
            # Uh-oh state (undefined state)
            # Error handling
            pass
            
        self.runs += 1
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class Button:
    '''
    @brief      A pushbutton class
    @details    This class represents a button that the can be pushed by the
                patrons of the elevator to move between floors.
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Button object
        @param pin  A pin object that the button is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('Button object created attached to pin '+ str(self.pin))

    
    def getButtonState(self):
        '''
        @brief      Gets the button state.
        @details    Since there is no hardware attached this method
                    returns a randomized True or False value.
        @return     A boolean representing the state of the button.
        '''
        return choice([True, False])
    
    def clear(self):
        '''
        @brief      Clears the button state.
        @details    Since there is no hardware attached this method
                    returns a False value.
        @return     A boolean representing the state of the button.
        '''
        return False
    
    def On(self):
        '''
        @brief      Sets the button state to True.
        @details    Since there is no hardware attached this method
                    returns a True value.
        @return     A boolean representing the state of the button.
        '''
        return True

class MotorDriver:
    '''
    @brief      A motor driver.
    @details    This class represents a motor driver used to make the elevator
                move up and down between floors.
    '''
    
    def __init__(self):
        '''
        @brief Creates a MotorDriver Object
        '''
        pass
    
    def Move_Up(self):
        '''
        @brief Moves the motor up
        '''
        print('Motor moving Up')
        
    
    def Move_Down(self):
        '''
        @brief Moves the motor down
        '''
        print('Motor moving Down')
        
    
    def Stop(self):
        '''
        @brief Stops the motor
        '''
        print('Motor Stopped')
        