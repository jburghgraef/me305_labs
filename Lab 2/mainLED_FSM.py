'''
@file main.py
'''

from LED_FSM import LED, TaskRealLED, TaskVirtualLED

## Virtual LED object
VirtualLED = LED('PB8')

## Real LED Object
RealLED = LED('Pin_A5')

## Task object to run Virtual LED
task1 = TaskVirtualLED(1, VirtualLED)

## Task Object to run Real LED on Nucleo
task2 = TaskRealLED(1, RealLED)

for N in range(10000): 
    task1.runVLED()
    task2.runRealLED()