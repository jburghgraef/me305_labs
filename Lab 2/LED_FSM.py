'''
@file LED_FSM.py

This file is an implementation of a finite-state-machine using Python. 
The example will implement some code to control a real LED using PWM 
and a virtual LED.

The Real LED will blink according to a sinusoidal wave - it will "fade"
in and out.

The Virtual LED will simply print when the LED turns on and when it 
turns off.

This file also demonstrates the ability of two finite-state-machines 
running simultaneously whilst running in Python.
'''

import math
import pyb
import utime

class TaskVirtualLED:
    '''
    @brief      A finite state machine to control a Virtual LED.
    @details    This class implements a finite state machine to control the
                operation of a Virtual LED blinking On and Off.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_VIRTUAL_LED_ON   = 1
    
    ## Constant defining State 2
    S2_VIRTUAL_LED_OFF  = 2
    
    def __init__(self, interval, VirtualLED):
        '''
        @brief      Creates a TaskVirtualLED object.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## An LED object used for Virtual LEDs
        self.VirtualLED = VirtualLED
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
    def runVLED(self):
        '''
        @brief      Runs one iteration of the task
        '''
        self.curr_time = utime.ticks_us()    #updating the current timestamp
            
        # checking if the timestamp has exceeded our "scheduled" timestamp
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if(self.state == self.S0_INIT):
                # Run State 0 Code
                self.transitionTo(self.S1_VIRTUAL_LED_ON)
                
            elif(self.state == self.S1_VIRTUAL_LED_ON):
                print('Virtual LED on')
                # Run State 1 Code
                self.VirtualLED.setLEDon()
                self.transitionTo(self.S2_VIRTUAL_LED_OFF)                
            
            elif(self.state == self.S2_VIRTUAL_LED_OFF):
                print('Virtual LED off')
                # Run State 2 Code
                self.VirtualLED.setLEDoff()
                self.transitionTo(self.S1_VIRTUAL_LED_ON)        
                        
            else:
                # Uh-oh state (undefined state)
                # Error handling
                pass
                
            self.runs += 1
            
            # updating the "Scheduled" timestamp
            self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
        
class TaskRealLED:
    '''
    @brief      A finite state machine to control a Real LED.
    @details    This class implements a finite state machine to control the
                operation of a Real LED on a Nucleo with PWM. The output
                changes the LED's brightness according to a sinusoid.
    '''
    
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0   
    
    ## Constant defining State 1
    S1_REAL_LED_ON      = 1
    
    def __init__(self, interval, RealLED):
        '''
        @brief      Creates a TaskLED object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## An LED object used for Virtual LEDs
        self.RealLED = RealLED
        
        ## A counter showing the number of times the task has run
        self.runs = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
    
        ## The interval of time, in microseconds, between runs of the task
        self.interval = int(interval*1e6)         
    
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def runRealLED(self):
            '''
            @brief      Runs one iteration of the task
            '''
            self.curr_time = utime.ticks_us()    #updating the current timestamp
            
            # checking if the timestamp has exceeded our "scheduled" timestamp
            if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
                if(self.state == self.S0_INIT):
                    # Run State 0 Code
                    self.transitionTo(self.S1_REAL_LED_ON)
                
                elif(self.state == self.S1_REAL_LED_ON):
                    # Run State 1 Code
                    
                    ## Initialize the pin
                    pin_A5 = pyb.Pin(pyb.Pin.cpu.A5, mode=pyb.Pin.OUT_PP)
                    
                    ## Initialize the timer
                    tim2 = pyb.Timer(2, freq = 20000)
                    
                    ## Initialize the Channel
                    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pin_A5)

                    ## x is the input into the sinusiodal function
                    self.x = self.curr_time*math.pi*2                   

                    ## val is the input value for the pusle width
                    self.val = 100*(0.5+math.sin(self.x)/2) 
                    
                    # Output the pulse width                    
                    t2ch1.pulse_width_percent(self.val)    
                    
                else:
                    # Uh-oh state (undefined state)
                    # Error handling
                    pass
                
                self.runs += 1
                
                # updating the "Scheduled" timestamp
                self.next_time = utime.ticks_add(self.start_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState
    
class LED:
    '''
    @brief      An LED class
    @details    This class represents an LED that can either be real - on the
                Nucleo Board - or virtual (generated by python).
    '''
    
    def __init__(self, pin):
        '''
        @brief      Creates a Virtual LED object
        @param pin  A pin object that the LED is connected to
        '''
        
        ## The pin object used to read the Button state
        self.pin = pin
        
        print('LED object created attached to pin '+ str(self.pin))

    
    def getLEDState(self):
        '''
        @brief      Gets the LED state.
        @details    This method returns the state of the LED.
        @return     A boolean representing the state of the LED.
        '''
        return self.LEDstate
    
    
    def setLEDon(self):
        '''
        @brief      Sets the LED state to on.
        @details    This method sets the internal variable LEDstate to be 
                    true, thus turning the LED on.
        '''
        self.LEDstate = True
        
        
    def setLEDoff(self):
        '''
        @brief      Sets the Virtual LED state to off.
        @details    This method sets the internal variable LEDstate to be 
                    false, thus turning the LED off.
        '''
        self.LEDstate = False
