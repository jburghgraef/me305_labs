'''
@file       fibb_gen.py
@brief      A file containing a function that generates fibonacci numbers.
@details    This file generates fibonacci numbers based off of a user-inputted
            index value. The user inputs the index of the desired fibonacci
            number, and then the fib() function uses that input to generate
            an array of fibonacci numbers ending in the value of the 
            desired fibonacci number. The link to the source code 
            repository is: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%201/fibb_gen.py
@author Jacob Burghgraef
@date September 29, 2020
'''

import numpy as np

#  A package containing the Numerical Python library
#
#  The NumPy Library provides a blank-slate array of variable size to generate
#  and store the Fibonacci sequence.
#  @author NumPy Developers
#  @copyright (c) 2008 - 2020
#  @date 2005

## Input assignment and initial check
#
#  The following code prompts the user to enter the index of the desired 
#  Fibonacci number and then assigns it to string idx, which will be passed to
#  the fib() function as a parameter. Then, a try-catch statement checks to 
#  the string idx to ensure that the provided input can be converted to an 
#  integer. If the value of idx is invalid, the loop prompts the user to re-
#  input a valid index value.
while True:
    idx = input('Please input an index for your desired Fibbonacci Number: ')
    try:
        check = int(idx);
        break
    except ValueError:
        print("Oops, that wasn't an integer.")
        print("Please input an integer greater than or equal to 0.")
        print("Note: the sequence is not defined for n<0.")

## Secondary check
#
#  This loop checks to make sure that the provided idx int value is a valid 
#  index in the fibonacci sequence; that is, that the idx is nonnegative.
while int(idx) < 0:    
    print("Oops, that integer was invalid.")
    print("Note: the sequence is not defined for n<0.")
    idx = input("Please input an integer greater than or equal to 0: ")

'''
@brief    A function that generates the Fibonacci Sequence

@details  The fib() function uses the passed (and verified) parameter idx as a string
          input for the index value of the Fibonacci number to be generated. It then
          converts the string idx to an integer 'index' using the int() function.
          Thereafter, index's value is checked, and if it's either 0 or 1, then the 
          Fibonacci numbers are directly printed out to the console. Otherwise, an
          array "fib_nums" is populated using the imported NumPy library function 
          "zeros()" at the length of one greater than the numerical value of index.
          This ensures that the desired Fibonacci number is actually generated, since
          arrays start at zero in Python. Then, in accordance with the definition of
          the Fibonacci sequence, the second value in the array fib_nums is set to 1,
          and the dummy index 'i' is set to 2, and the Fibonacci sequence up to the
          specified index is generated within the while loop. Finally, the final 
          value in fib_nums is converted to an integer and then to a string (since
          zeros uses floating point values) and then printed to the console.
'''
def fib (idx):        
    
    index = int(idx);    
    
    print ('Calculating Fibonacci number at '
       'index n = {:}.'.format(idx))
    if index == 0:
        print('The Fibonacci number is: 0')
    elif index == 1:
        print('The Fibonacci number is: 1')
    else:
        fib_nums = np.zeros(index+1)
        fib_nums[1] = 1;
        i = 2;
        while (i <= index):
            fib_nums[i] = fib_nums[i-1] + fib_nums[i-2];
            i += 1;
        print('The Fibonacci number is: ' + str(int(fib_nums[index])))

## Runs the fib() function at the specified index
fib(idx)    

## The following code is for debugging purposes.

#if __name__ == '__main__':
#     fib(0)
#     fib(1)
#     fib(10)
#     fib(100)