'''
@file main.py
@package Lab_7
A Main Script for running Lab 7

@author Jacob Burghgraef

@date December 4th, 2020

The documentation for this code can be found at: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%207/lab7main.py
'''

from Task_Controller_Lab7 import Controller
from Task_User_Lab7 import TaskUser

## Create a User task object
task1 = TaskUser(.02) # Interval of 20 milliseconds

## Create a Controller task object
task2 = Controller(.02) # Time step of 20 milliseconds

while True:
    task1.run()
    task2.run()