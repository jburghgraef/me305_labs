'''
@file Task_User_Lab7.py
@package Lab_7
@brief A User interface task.
@details This is a user interface task that sends measured motor speed values, 
         the position values and the timestamps from Task_Controllor_Lab7.py to 
         UI_FrontEnd_Lab7.py; it also receives kp values from the frontend UI.
         Additionally, it calculates the performance metric J using the 
         measured values from the controller task and reference values from 
         the reference csv file.

@author Jacob Burghgraef

@date December 4, 2020
 
Below is an image of the Finite State Machine describing this file
@image html "ME 305 Lab 7 Task_User FSM.jpg"

The source code for this file can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%207/Task_User_Lab7.py
'''

import shares
from pyb import UART
import utime
from array import array

class TaskUser:
    '''
    User interface task.
    '''
    
    ## Initialization state
    S0_INIT                     = 0
    
    ## Waiting for reference values state
    S1_WAIT_FOR_REF_VALUES      = 1
    
    ## Waiting for kp state
    S2_WAIT_FOR_KP              = 2
    
    ## Waiting for response state
    S3_WAIT_FOR_RESP            = 3
    
    ## Sending the performance metric state
    S4_SEND_J                   = 4
    
    def __init__(self, interval):
        '''
        Creates a TaskUser object.
        '''
        
        ##  The amount of time in microseconds between runs of the task
        self.interval = int(interval*1e6)
    
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Counter that describes the number of times the task has run
        self.runs = 0
        
        ## The time array
        self.t = array('i')
        
        ## The array of angular velocities
        self.w = array('f')
        
        ## The array of measured positions
        self.th = array('f')
        
        ## The array of reference velocities
        self.w_ref = array('i')
        
        ## The array of reference positions
        self.th_ref = array('f')
        
        ## The size of the reference value array
        self.size = 0
        
        ## The J value
        self.J = 0
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us()
        
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)

        ## Serial port
        self.ser = UART(2)
        
        ## The time at the beginning of Data Collection
        self.collect_time = 0
        
    def run(self):
        '''
        Runs one iteration of the task
        '''
        ## The timestamp for the current iteration (or most recent iteration) of the task. This is with respect to an arbitrary initial time and must be used differentially.
        self.curr_time = utime.ticks_us()
        if utime.ticks_diff(self.curr_time, self.next_time) >= 0:
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_WAIT_FOR_REF_VALUES)
                
            elif (self.state == self.S1_WAIT_FOR_REF_VALUES):
                if self.ser.any():
                    # Read each available line of reference values
                    while True:
                        self.line = self.ser.readline().decode('ascii')
                        v = self.line.strip()
                        
                        self.line = self.ser.readline().decode('ascii')
                        x = self.line.strip()
                            # If the line is empty, there are no more rows so exit the loop
                        if self.line == '':
                            break
    
                    # If the line is not empty, strip special characters, split on commas, and
                    # then append each value to its list.
                        else:
                            (t,v,x) = self.line.strip().split(',');
                            self.w_ref.append(float(v))
                            self.th_ref(float(x))
                if len(self.w_ref >= 751):
                    self.transitionTo(self.S2_WAIT_FOR_KP)
                                
            elif (self.state == self.S2_WAIT_FOR_KP):
                if self.ser.any():
                    self.transitionTo(self.S3_WAIT_FOR_RESP)
                    # Send Kp to controller task using shares
                    shares.kp = self.ser.readline().decode('ascii')
                    # Send the first reference velocity to the controller task
                    shares.w_ref = self.w_ref[0]
                    # Set the count for the reference value index to 1
                    self.count = 1
                    # Initialize the Start time for data collection
                    self.collect_time = utime.ticks_add(self.curr_time, self.interval)
                    
            elif (self.state == self.S3_WAIT_FOR_RESP):
                if shares.w_meas and shares.theta:
                    # Send the new reference value
                    shares.w_ref = self.w_ref[self.count]
                    # Append the current time to the time array
                    self.t.append(int(utime.ticks_diff(self.curr_time, self.collect_time)))
                    # Append the measured angular velocity to the velocity array
                    self.w.append(int(shares.w_meas))
                    # Append the measured angular position to the position array
                    self.th.append(int(shares.theta))
                    # Write the time value and omega value to the serial port
                    self.ser.write('{:}, {:}, {:}\r\n'.format (self.t[-1], self.w[-1], self.th[-1]))
                    
                    # Reset the shares.w_meas value
                    shares.w_meas = None
                    
                    # Reset the shares.th_meas value
                    shares.theta = None
                    
                    # Increment the counter
                    self.count += 1
                    
                if (shares.kp == None):
                    self.transitionTo(self.S4_SEND_J)
                    self.collect_time = 0
                
            elif (self.state == self.S4_SEND_J):
                # Calculate the J value
                K = len(self.w)
                for k in range(K):
                    self.J += (int(self.w_ref[k]) - int(self.w[k]))**2 + (self.th_ref[k] - self.th[k])**2
                # Write the Performance Metric to the serial port
                self.J = self.J/K
                self.ser.write('{:}\r\n'.format(self.J))
                self.transitionTo(self.S1_WAIT_FOR_REF_VALUES)
            
            else:
                # Invalid state case, error occurs
                pass
            
            # Specifying the next time the task will run
            self.next_time = utime.ticks_add(self.next_time, self.interval)
    
    def transitionTo(self, newState):
        '''
        Updates the state variable
        '''
        self.state = newState