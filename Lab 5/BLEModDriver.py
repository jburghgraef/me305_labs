'''
@file BLEModDriver.py

@author Jacob Burghgraef

@date November 10, 2020

This file is the driver for the HM 11 Bluetooth Module.

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%205/BLEModDriver.py
'''

import pyb
from pyb import UART

class BLEDriver:
    '''
    @brief This is the class for the bluetooth driver
    @details    The BLEDriver class has 5 functions - wait4Char(), which waits
                for a character on the UART Bus; readChar(), which reads the 
                line of input on the UART Bus; writeChar(), which writes a 
                character to the UART Bus; LEDon(), which turns the LED 
                connected to PinA5 on; and LEDoff(), which turns the same LED
                off.
    '''

    def __init__(self):
        '''
        @brief Sets up the driver for the Bluetooth Module
        '''

        ## The uart bus
        self.uart = UART(3, 9600)

        ## The relevant pin corresponding the LED
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)

    def wait4Char(self):
        '''
        @brief Waits for a character on the UART bus
        @return check A boolean variable that represents the presence of a 
                character waiting on the UART bus or not. Only returns check when
                check is true - that is, when a character is detected in UART.
        '''
        if self.uart.any() != 0:
            self.check = True
        else:
            self.check = False
        return self.check

    def readChar(self):
        '''
        @brief Reads a character from the UART bus
        @return The line of characters read from the UART bus
        '''
        return self.uart.readline()

    def writeChar(self, char):
        '''
        @brief Writes a character to the UART bus
        @param char The character input written to the UART bus
        '''
        self.wchar = char
        self.uart.writeChar(self.wchar)

    def LEDon(self):
        '''
        @brief Turns the LED on
        '''
        self.pinA5.high()

    def LEDoff(self):
        '''
        @brief Turns the LED off
        '''
        self.pinA5.low()