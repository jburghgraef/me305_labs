'''
@file Lab5UITask.py

@author Jacob Burghgraef

@date November 10th, 2020

This file is a task that creates a user interface that interacts with a bluetooth module.

Below is an image of the FSM for this UI Task

@image html "lab5UITaskFSM.jpg"

Below is an image of the Task Diagram describing the communication between this
file and the mobile phone application.

@image html "ME 305 Lab 5 Task Diagram.jpg"

The documentation can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%205/Lab5UITask.py
'''

from BLEModDriver import BLEDriver
import utime

class UITaskL5:
    '''
    @brief  Creates a UI Task Object that interacts with a mobile app.
    @details    This UI task reads frequency input through the DSD Tech HM11 
                Bluetooth module sent from a mobile app on a mobile Android or 
                iOS device and blinks the LED on the Nucleo at that frequency.
                Only integer values between 1 and 10 Hz are accepted. The task
                has been implemented as a finite state machine with 3 states -
                checking for input, turning the LED on, and turning the LED 
                off.
    '''
   
    ## Constant defining State 0 - Initialization
    S0_INIT             = 0
    
    ## Constant defining State 1
    S1_CHECK_FOR_INPUT  = 1
    
    ## Constant defining State 2
    S2_LED_ON           = 2
    
    ## Constant defining State 3
    S3_LED_OFF          = 3
    
    def __init__(self, freq):
        '''
        @brief      Creates a UITaskL5 object.
        '''
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ## Initialize check of number of waiting input bytes to equal 0
        self.check = 0
        
        ## The BLE Module Driver Object
        self.ble = BLEDriver()
        
        ## Initial Time interval between Data Samplings, in microseconds
        self.interval = int((1/(freq))*5e5)
        
        ## The timestamp for the first iteration
        self.start_time = utime.ticks_us() # The microsecond timestamp for the task starting
        
        ## The "timestamp" for when to run the task next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        
    def run(self):
        '''
        @brief Runs the UI Task
        '''
        
        self.curr_time = utime.ticks_us()    #updating the current timestamp
        
        if (utime.ticks_diff(self.curr_time, self.next_time) >= 0):
            if (self.state == self.S0_INIT):
                self.transitionTo(self.S1_CHECK_FOR_INPUT)
        
            elif (self.state == self.S1_CHECK_FOR_INPUT):
                # Check for any input
                self.check = self.ble.wait4Char()
                if (self.check):
                    self.new_freq = self.ble.readChar()
                    
                    # Check to make sure the inputted value is an integer
                    try:
                        self.freq = int(self.new_freq);
                    except ValueError:
                        print('Error, invalid frequency value.')
                        print('The frequency must be an integer between 1 and 10 Hz.')
                    
                    # Check to Make sure the frequency is between 1 and 10 Hz
                    if (self.freq > 10 or self.freq < 1):
                        # If not, throw an error
                        print('Error, invalid frequency value.')
                        print('The frequency must be an integer between 1 and 10 Hz.')
                    else:
                        # Redefine the interval to be the new frequency
                        self.interval = int((1/(self.freq))*5e5)
                        print('LED Blinking at ', self.freq, ' Hz')
                        # Switch to On state for the LED
                        self.transitionTo(self.S2_LED_ON)
                else: 
                    pass
        
            elif (self.state == self.S2_LED_ON):
                # Check for any input
                self.check = self.ble.wait4Char()
                if (self.check):
                    self.ble.LEDoff()
                    self.transitionTo(self.S1_CHECK_FOR_INPUT)
                else: 
                    self.ble.LEDon()
                    self.transitionTo(self.S3_LED_OFF)
                
            elif (self.state == self.S3_LED_OFF):
                # Check for any input
                self.check = self.ble.wait4Char()
                if (self.check):
                    self.ble.LEDoff()
                    self.transitionTo(self.S1_CHECK_FOR_INPUT)
                else: 
                    self.ble.LEDoff()
                    self.transitionTo(self.S2_LED_ON)
                
            else:
                # Undefined State, Error
                pass
                
            self.next_time = utime.ticks_add(self.curr_time, self.interval)
        
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState