'''
@file lab5main.py

@author Jacob Burghgraef

@date November 10, 2020

This file serves as the main script for running the scripts of lab 5.

The documentation for this lab can be found here: https://bitbucket.org/jburghgraef/me305_labs/src/master/Lab%205/lab5main.py
'''

from Lab5UITask import UITaskL5

## A Lab5UITask object (start with a frequency of 10 Hz)
task = UITaskL5(10)

while True:
    task.run()